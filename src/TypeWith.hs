{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
module TypeWith where

import           Data.Functor.Classes
import           Data.Proxy
import           GHC.Exts
import           Test.QuickCheck

data Some (c :: * -> Constraint) where
  Some
    :: (c a, Show a, Eq a, Arbitrary a, CoArbitrary a, Function a)
    => Proxy a
    -> Some c

genValue :: Arbitrary a => Proxy a -> Gen a
genValue (_ :: Proxy a) = arbitrary @a

genFun
  :: (Function a, Arbitrary a, Arbitrary b, CoArbitrary a)
  => Proxy a
  -> Proxy b
  -> Gen (Fun a b)
genFun (_ :: Proxy a) (_ :: Proxy b) =
  arbitrary @(Fun a b)

eqTypes :: [Some Eq]
eqTypes = [Some (Proxy @Bool), Some (Proxy @Int), Some (Proxy @String)]

data Some1 (c :: (* -> *) -> Constraint) where
  Some1 :: (c f, Eq1 f, Show1 f, Arbitrary1 f) => Proxy f -> Some1 c

genFunctor :: (Arbitrary1 f, Arbitrary a) => Proxy f -> Proxy a -> Gen (f a)
genFunctor _ _ = liftArbitrary arbitrary

functors :: [Some1 Functor]
functors = [Some1 (Proxy @[]), Some1 (Proxy @Maybe)]

-- * Test case example

data FunctorAssociativity where
  FunctorAssociativity
    :: (Show1 f, Eq1 f, Show a, Show b, Show c, Eq c, Functor f)
    => f a
    -> Fun a b
    -> Fun b c
    -> FunctorAssociativity

instance Show FunctorAssociativity where
  showsPrec p (FunctorAssociativity xs f1 f2) =
    ("FunctorAssociativity (" <>)
      . liftShowsPrec showsPrec showList p xs
      . (") " <>)
      . showsPrec p f1
      . (" " <>)
      . showsPrec p f2

instance Arbitrary FunctorAssociativity where
  arbitrary = do
    Some ta <- elements eqTypes
    Some tb <- elements eqTypes
    Some tc <- elements eqTypes
    Some1 tf <- elements functors
    FunctorAssociativity
      <$> genFunctor tf ta
      <*> genFun ta tb
      <*> genFun tb tc

prop_functor_associativity (FunctorAssociativity xs (Fun _ f1) (Fun _ f2)) =
  fmap (f2 . f1) xs `eq1` fmap f2 (fmap f1 xs)

main = quickCheck prop_functor_associativity

-- can also do in GHCi:
-- > sample' (arbitrary @FunctorAssociativity)
